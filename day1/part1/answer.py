f = open("input.txt", "r")
lines = f.readlines()
f.close()

results = []
tempAdd = 0
for line in lines:
  if line == "\n":
    results.append(tempAdd)
    tempAdd = 0
  else:
    tempAdd += int(line)

print(max(results))
