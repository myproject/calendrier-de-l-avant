def getPointWin(ePlay, pPlay):
  points = 0
  if ePlay == "Pierre":
    if pPlay == "Pierre":
      points += 3
    elif pPlay == "Feuille":
      points += 6
    else:
      points += 0
  elif ePlay == "Feuille":
    if pPlay == "Pierre":
      points += 0
    elif pPlay == "Feuille":
      points += 3
    else:
      points += 6
  else:
    if pPlay == "Pierre":
      points += 6
    elif pPlay == "Feuille":
      points += 0
    else:
      points += 3

  return points

def getPointsByPlay(pPlay):
  points = 0
  if pPlay == "Pierre":
    points += 1
  elif pPlay == "Feuille":
    points += 2
  else:
    points += 3

  return points

f = open("input.txt", "r")
lines = f.readlines()
f.close()

ennemy = {"A": "Pierre", "B": "Feuille", "C": "Scisceaux"}
player = {"X": "Pierre", "Y": "Feuille", "Z": "Scisceaux"}

scores = 0
for line in lines:
  bPlay, pPlay = line.split(" ")
  pPlay = pPlay.strip("\n")
  scores += getPointWin(ennemy[bPlay], player[pPlay])
  scores += getPointsByPlay(player[pPlay])

print(scores)