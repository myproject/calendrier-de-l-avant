def getPointWin(ePlay, pPlay):
  points = 0
  if ePlay == "Pierre":
    if pPlay == "Pierre":
      points += 3
    elif pPlay == "Feuille":
      points += 6
    else:
      points += 0
  elif ePlay == "Feuille":
    if pPlay == "Pierre":
      points += 0
    elif pPlay == "Feuille":
      points += 3
    else:
      points += 6
  else:
    if pPlay == "Pierre":
      points += 6
    elif pPlay == "Feuille":
      points += 0
    else:
      points += 3

  return points

def getPointsByPlay(pPlay):
  points = 0
  if pPlay == "Pierre":
    points += 1
  elif pPlay == "Feuille":
    points += 2
  else:
    points += 3

  return points

def getMove(ePlay, condition):
  move = ""
  if condition == "Egalite":
    move = ePlay
  elif condition == "Perdre":
    if ePlay == "Pierre":
      move = "Scisceaux"
    elif ePlay == "Feuille":
      move = "Pierre"
    else:
      move = "Feuille"
  else:
    if ePlay == "Pierre":
      move = "Feuille"
    elif ePlay == "Feuille":
      move = "Scisceaux"
    else:
      move = "Pierre"
 
  return move

f = open("input.txt", "r")
lines = f.readlines()
f.close()

ennemy = {"A": "Pierre", "B": "Feuille", "C": "Scisceaux"}
win = {"X": "Perdre", "Y": "Egalite", "Z": "Gagner"}

scores = 0
for line in lines:
  bPlay, pPlay = line.split(" ")
  condition = pPlay.strip("\n")

  move = getMove(ennemy[bPlay], win[condition])
  scores += getPointWin(ennemy[bPlay], move)
  scores += getPointsByPlay(move)

print(scores)
